const express = require('express');
const path = require('path');


const NODE_PORT = 8080;
const CLIENT_FOLDER = path.join(__dirname, '../client/');
const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages')

var app = express();

//routes
// app.use is the first catch - to static 
app.use(express.static(CLIENT_FOLDER));

// ERROR HANDLERS
// Function to handle resource not found
app.use(function(req, res) {
  res
    .status(404)
    .sendFile(path.join(MSG_FOLDER, '/404.html'));
    // sends content of file 
});


// Function to handle server error
app.use(function(err, req, res, next) {
  console.log(err);
  res
    .status(500)
    .sendFile(path.join(MSG_FOLDER, "/500.html"));
});



app.listen(NODE_PORT, function(){
    console.log("Server running at localhost:" + NODE_PORT);
});


